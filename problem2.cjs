const inventory = require('./inventory.cjs');
function problem2(inventory) {
    if (arguments.length !== 1 || inventory.length === 0 || !Array.isArray(inventory)) {
        return [];
    }
    return inventory[inventory.length - 1];
}
module.exports = problem2;