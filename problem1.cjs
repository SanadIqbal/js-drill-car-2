const inventory = require('./inventory.cjs');
function problem1(inventory, carId) {
   if (arguments.length != 2 || inventory.length === 0 || !Array.isArray(inventory) || typeof carId !== 'number') {
      return [];
   }
   return inventory.filter(function(car){
      return car.id === carId;
   });
   return [];
}
module.exports = problem1;