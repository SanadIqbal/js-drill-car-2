const inventory = require('./inventory.cjs');
const problem4 = require('./problem4.cjs');
const result = problem4(inventory);
function problem5(result) {
    if (arguments.length !== 1 || result.length === 0 || !Array.isArray(result)) {
        return 0;
    }
    const year = result.filter(function (car) {
        return car < 2000;
    });
    return year.length;
}
module.exports = problem5;