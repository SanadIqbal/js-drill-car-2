const inventory = require('./inventory.cjs');
function problem3(inventory) {
    if (arguments.length !== 1 || inventory.length === 0 || !Array.isArray(inventory)) {
        return [];
    }
    const car = [];
    inventory.forEach(element => {
        car.push(element.car_model);
    });
    return car.sort();
}

module.exports = problem3;