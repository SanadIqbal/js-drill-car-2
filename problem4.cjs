const inventory = require('./inventory.cjs');
function problem4(inventory) {
    if (arguments.length !== 1 || inventory.length === 0 || !Array.isArray(inventory)) {
        return [];
    }
    const year = [];
    inventory.forEach(function (car) {
        year.push(car.car_year);
    });
    return year;
}

module.exports = problem4;