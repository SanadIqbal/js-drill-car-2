const inventory = require('./inventory.cjs');
function problem6(inventory) {
    if (arguments.length !== 1 || inventory.length === 0 || !Array.isArray(inventory)) {
        return [];
    }
    const cars = inventory.filter(function (car) {
        return (car.car_make === 'BMW' || car.car_make === 'Audi');
    });
    return JSON.stringify(cars);
}
module.exports = problem6;